package com.gitrekt.quora.server.middlewares;

import com.gitrekt.quora.exceptions.InvalidFile;
import com.gitrekt.quora.exceptions.MethodNotAllowed;
import com.gitrekt.quora.mediaclient.MinioMediaServer;
import com.gitrekt.quora.models.Response;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;
import io.netty.handler.codec.http.multipart.DefaultHttpDataFactory;
import io.netty.handler.codec.http.multipart.FileUpload;
import io.netty.handler.codec.http.multipart.HttpDataFactory;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;

import java.io.IOException;
import java.util.UUID;

public class FileUploadMiddleware extends SimpleChannelInboundHandler<HttpObject> {
  private static final HttpDataFactory factory = new DefaultHttpDataFactory(true);
  private HttpRequest httpRequest;
  private HttpPostRequestDecoder httpPostRequestDecoder;
  private String fileName;
  private String extension;

  @Override
  protected void channelRead0(ChannelHandlerContext ctx, HttpObject msg) throws Exception {

    if (msg instanceof HttpRequest) {
      httpRequest = (HttpRequest) msg;
      if (!httpRequest.uri().equals("/upload")) {
        ctx.fireChannelRead(msg);
        return;
      }
      if (!httpRequest.method().equals(HttpMethod.POST)) {
        throw new MethodNotAllowed();
      }

      httpPostRequestDecoder = new HttpPostRequestDecoder(factory, httpRequest);
      httpPostRequestDecoder.setDiscardThreshold(0);
      extension = "";
    }

    if (httpPostRequestDecoder != null) {
      if (msg instanceof HttpContent) {
        final HttpContent chunk = (HttpContent) msg;
        httpPostRequestDecoder.offer(chunk);
        readChunk(ctx);

        if (msg instanceof LastHttpContent) {

          // write response
          Response response = new Response();
          response.setUrl(fileName);
          ctx.write(response);
          if (!HttpUtil.isKeepAlive(httpRequest)) {
            ctx.writeAndFlush(Unpooled.EMPTY_BUFFER).addListener(ChannelFutureListener.CLOSE);
          }
          resetPostRequestDecoder();
        }
      }
    }
  }

  private void readChunk(ChannelHandlerContext ctx) throws IOException, InvalidFile {
    while (httpPostRequestDecoder.hasNext()) {
      InterfaceHttpData data = httpPostRequestDecoder.next();
      if (data != null) {
        try {
          switch (data.getHttpDataType()) {
            case Attribute:
              break;
            case FileUpload:
              FileUpload fileUpload = (FileUpload) data;
              extension = getFileExtension(fileUpload.getFilename());
              try {
                fileName = MinioMediaServer.getInstance()
                    .uploadMedia(fileUpload.getFile().getAbsolutePath(), extension);
              } catch (Exception exception) {
                if (exception instanceof InvalidFile) {
                  throw new InvalidFile();
                } else {
                  exception.printStackTrace();
                }
              }
              break;
            default:
              break;
          }
        } finally {
          data.release();
        }
      }
    }
  }

  /**
   * Extract file extension from file name.
   *
   * @param name name of the file.
   * @return file extension.
   */
  private String getFileExtension(String name) {
    int extId = name.lastIndexOf('.');
    String extension = "";
    if (extId > 0) {
      extension = name.substring(extId);
    }
    return extension;
  }

  /** Reset decoder after read is finished. */
  private void resetPostRequestDecoder() {

    httpRequest = null;
    httpPostRequestDecoder.destroy();
    httpPostRequestDecoder = null;
    fileName = "";
  }

  @Override
  public void channelInactive(ChannelHandlerContext ctx) throws Exception {
    if (httpPostRequestDecoder != null) {
      httpPostRequestDecoder.cleanFiles();
    }
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    ctx.channel().writeAndFlush(cause).addListener(ChannelFutureListener.CLOSE);
  }
}
