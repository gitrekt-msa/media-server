package com.gitrekt.quora.server;

import com.gitrekt.quora.server.middlewares.FileServerMiddleware;
import com.gitrekt.quora.server.middlewares.FileUploadMiddleware;
import com.gitrekt.quora.server.middlewares.JsonEncoder;
import com.gitrekt.quora.server.middlewares.StaticFileResponseWriter;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpContentCompressor;
import io.netty.handler.codec.http.HttpMethod;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.cors.CorsConfig;
import io.netty.handler.codec.http.cors.CorsConfigBuilder;
import io.netty.handler.codec.http.cors.CorsHandler;
import io.netty.handler.stream.ChunkedWriteHandler;

public class HttpServerInitializer extends ChannelInitializer<SocketChannel> {

  @Override
  protected void initChannel(SocketChannel socketChannel) throws Exception {
    CorsConfig corsConfig =
        CorsConfigBuilder.forAnyOrigin()
            .allowedRequestHeaders("X-Requested-With", "Content-Type", "Content-Length")
            .allowedRequestMethods(
                HttpMethod.GET,
                HttpMethod.POST,
                HttpMethod.PUT,
                HttpMethod.DELETE,
                HttpMethod.OPTIONS)
            .build();

    ChannelPipeline pipeline = socketChannel.pipeline();
    pipeline.addLast(new CorsHandler(corsConfig));
    pipeline.addLast("codec", new HttpServerCodec());
    pipeline.addLast(new JsonEncoder());
    pipeline.addLast(new ChunkedWriteHandler());
    pipeline.addLast(new StaticFileResponseWriter());
    pipeline.addLast("compressor", new HttpContentCompressor());
    pipeline.addLast(new FileUploadMiddleware());
    pipeline.addLast(new FileServerMiddleware());

  }
}
