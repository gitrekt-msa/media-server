//package com.gitrekt.quora.media;
//
//import com.gitrekt.quora.mediaclient.GlusterFsClient;
//import org.junit.Test;
//
//import java.io.IOException;
//import java.nio.file.FileAlreadyExistsException;
//import java.util.Arrays;
//import java.util.Random;
//import java.util.UUID;
//
//import static org.junit.Assert.*;
//
//public class GlusterFsClientTests {
//
//  @Test
//  public void shouldCreateFile() throws IOException {
//    GlusterFsClient client = GlusterFsClient.getInstance();
//    String fileName = UUID.randomUUID().toString();
//    client.createFile(fileName);
//    assertTrue(client.fileExists(fileName));
//  }
//
//  @Test(expected = FileAlreadyExistsException.class)
//  public void shouldThrowExceptionWhenFileExists() throws IOException {
//    GlusterFsClient client = GlusterFsClient.getInstance();
//    String fileName = UUID.randomUUID().toString();
//    client.createFile(fileName);
//    client.createFile(fileName);
//  }
//
//  @Test
//  public void shouldReadAndWriteCorrectFileData() throws IOException {
//    GlusterFsClient client = GlusterFsClient.getInstance();
//
//    // generate random data
//    byte[] array = new byte[128];
//    new Random().nextBytes(array);
//
//    // random file name
//    String fileName = UUID.randomUUID().toString() + ".txt";
//
//    // write byte array
//    client.writeFile(fileName, array);
//
//    // read file
//    byte[] readFile = client.readFile(fileName);
//
//    // assert array content is not empty
//    byte[] emptyArray = new byte[128];
//    assertFalse(Arrays.equals(readFile, emptyArray));
//
//    // assert content is the same
//    assertArrayEquals(readFile, array);
//  }
//
//  @Test
//  public void shouldReturnNullWhenAllNodesDown() throws IOException {
//    GlusterFsClient client = GlusterFsClient.getInstance();
//
//    // modify hosts
//    String[] hosts = client.hosts;
//    client.hosts = new String[3];
//
//    byte[] result = client.readFile("name-should-not-matter");
//    assertNull(result);
//
//    // restore hosts for other tests
//    client.hosts = hosts;
//  }
//}
