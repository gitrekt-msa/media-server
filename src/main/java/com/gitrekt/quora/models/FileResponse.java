package com.gitrekt.quora.models;

import io.netty.handler.codec.http.HttpRequest;
import java.io.InputStream;

public class FileResponse {
  String fileName;
  HttpRequest request;
  InputStream data;

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public InputStream getData() {
    return data;
  }

  public void setData(InputStream data) {
    this.data = data;
  }

  public HttpRequest getRequest() {
    return request;
  }

  public void setRequest(HttpRequest request) {
    this.request = request;
  }
}
