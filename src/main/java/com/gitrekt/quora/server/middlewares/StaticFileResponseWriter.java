package com.gitrekt.quora.server.middlewares;

import com.gitrekt.quora.models.FileResponse;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import io.netty.handler.codec.http.*;

import io.netty.handler.stream.ChunkedStream;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import java.io.InputStream;
import java.net.URLConnection;

public class StaticFileResponseWriter extends ChannelOutboundHandlerAdapter {
  HttpChunkedInput httpChunkWriter;
  ChunkedStream chunkedStream;

  @Override
  public void write(final ChannelHandlerContext ctx, Object msg, ChannelPromise promise)
      throws Exception {

    if (msg instanceof FileResponse) {
      FileResponse fileResponse = (FileResponse) msg;
      String mimeType = URLConnection.guessContentTypeFromName(fileResponse.getFileName());

      InputStream stream = fileResponse.getData();

      DefaultHttpResponse response =
          new DefaultHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.OK);

      response.headers().set(HttpHeaderNames.TRANSFER_ENCODING, HttpHeaderValues.CHUNKED);
      response.headers().set(HttpHeaderNames.CONTENT_TYPE, mimeType + "; charset=UTF-8");
      ctx.write(response);

      chunkedStream = new ChunkedStream(stream);
      httpChunkWriter = new HttpChunkedInput(chunkedStream);
      ctx.writeAndFlush(httpChunkWriter).addListener(ChannelFutureListener.CLOSE);
    } else {
      ctx.writeAndFlush(msg).addListener(ChannelFutureListener.CLOSE);
    }
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    ctx.channel().writeAndFlush(cause).addListener(ChannelFutureListener.CLOSE);
  }

  @Override
  public void close(ChannelHandlerContext ctx, ChannelPromise promise) throws Exception {
    chunkedStream.close();
    httpChunkWriter.close();
  }
}
