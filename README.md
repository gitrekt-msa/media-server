# Media Server

Distributed FS using GlusterFS for Media files storage.
 
### Instructions

1. Clone the repo 

2. Run `docker-compose up -d` to start the server and all associated services

3. Access the server [http://localhost:5000](http://localhost:5000)