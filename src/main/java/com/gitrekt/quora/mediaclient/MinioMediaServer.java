package com.gitrekt.quora.mediaclient;

import com.gitrekt.quora.exceptions.InvalidFile;
import io.minio.MinioClient;
import io.minio.errors.*;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

public class MinioMediaServer {

  private MinioClient minioClient;
  private final String[] buckets = {"images", "videos"};
  public final String[] imagesExt = {".jpg", ".png", ".jpeg"};
  public final String[] videosExt = {".mp4", ".avi", ".flv", ".webm"};

  public MinioMediaServer() {
    try {
      minioClient =
          new MinioClient(
              System.getenv("MINIO_HOST"),
              9000,
              System.getenv("MINIO_ACCESS_KEY"),
              System.getenv("MINIO_SECRET_KEY"));
      initBuckets();
    } catch (Exception exception) {
      exception.printStackTrace();
    }
  }

  private void initBuckets()
      throws IOException, InvalidKeyException, NoSuchAlgorithmException, InsufficientDataException,
      InternalException, NoResponseException, InvalidBucketNameException,
      XmlPullParserException, ErrorResponseException, RegionConflictException {
    for (String bucket : buckets) {
      if (!minioClient.bucketExists(bucket)) {
        minioClient.makeBucket(bucket);
      }
    }
  }

  public String uploadMedia(String filename, String ext)
      throws IOException, XmlPullParserException, NoSuchAlgorithmException, InvalidKeyException,
      InvalidArgumentException, InternalException, NoResponseException,
      InvalidBucketNameException, InsufficientDataException, ErrorResponseException,
      InvalidFile {

    // is image
    boolean isImage = false;
    boolean isVideo = false;
    for (String imageExt : imagesExt) {
      if (ext.equals(imageExt)) {
        isImage = true;
        break;
      }
    }

    // is video
    for (String videoExt : videosExt) {
      if (ext.equals(videoExt)) {
        isVideo = true;
        break;
      }
    }

    if (isImage) {
      return uploadImage(filename, ext);
    } else if (isVideo) {
      return uploadVideo(filename, ext);
    } else {
      throw new InvalidFile();
    }
  }

  private String upload(String bucket, String filename, String extension)
      throws IOException, XmlPullParserException, NoSuchAlgorithmException, InvalidKeyException,
      InvalidArgumentException, InternalException, NoResponseException,
      InvalidBucketNameException, InsufficientDataException, ErrorResponseException {
    String mimeType = URLConnection.guessContentTypeFromName(filename);
    String newFileName = UUID.randomUUID().toString() + extension;
    minioClient.putObject(bucket, newFileName, filename, mimeType);
    return newFileName;
  }

  public String uploadImage(String filename, String extension)
      throws IOException, InvalidKeyException, NoSuchAlgorithmException, XmlPullParserException,
      InvalidArgumentException, ErrorResponseException, NoResponseException,
      InvalidBucketNameException, InsufficientDataException, InternalException {
    return upload("images", filename, extension);
  }

  public String uploadVideo(String filename, String extension)
      throws IOException, InvalidKeyException, NoSuchAlgorithmException, XmlPullParserException,
      InvalidArgumentException, ErrorResponseException, NoResponseException,
      InvalidBucketNameException, InsufficientDataException, InternalException {
    return upload("videos", filename, extension);
  }

  public InputStream getMedia(String filename)
      throws IOException, InvalidKeyException, NoSuchAlgorithmException, InsufficientDataException,
      InvalidArgumentException, InternalException, NoResponseException,
      InvalidBucketNameException, XmlPullParserException, ErrorResponseException, InvalidFile {

    String ext = getFileExtension(filename);

    // is image
    boolean isImage = false;
    boolean isVideo = false;
    for (String imageExt : imagesExt) {
      if (ext.equals(imageExt)) {
        isImage = true;
        break;
      }
    }

    // is video
    for (String videoExt : videosExt) {
      if (ext.equals(videoExt)) {
        isVideo = true;
        break;
      }
    }

    String bucketName = "";
    if (isImage) {
      bucketName = buckets[0];
    } else if (isVideo) {
      bucketName = buckets[1];
    } else {
      throw new InvalidFile();
    }

    return minioClient.getObject(bucketName, filename);
  }

  /**
   * Extract file extension from file name.
   *
   * @param name name of the file.
   * @return file extension.
   */
  private String getFileExtension(String name) {
    int extId = name.lastIndexOf('.');
    String extension = "";
    if (extId > 0) {
      extension = name.substring(extId);
    }
    return extension;
  }

  private static class MinioMediaServerHelper {

    private static final MinioMediaServer INSTANCE = new MinioMediaServer();
  }

  public static MinioMediaServer getInstance() {
    return MinioMediaServerHelper.INSTANCE;
  }
}
