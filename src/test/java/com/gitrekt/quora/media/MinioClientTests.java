package com.gitrekt.quora.media;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import com.gitrekt.quora.exceptions.InvalidFile;
import com.gitrekt.quora.mediaclient.MinioMediaServer;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidArgumentException;
import io.minio.errors.InvalidBucketNameException;
import io.minio.errors.NoResponseException;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Random;
import java.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.xmlpull.v1.XmlPullParserException;

public class MinioClientTests {

  MinioMediaServer client;

  @Before
  public void init() {
    client = MinioMediaServer.getInstance();
  }

  @Test
  public void shouldCreateFile()
      throws IOException, XmlPullParserException, NoSuchAlgorithmException, InvalidKeyException,
      InvalidArgumentException, InternalException, NoResponseException, InvalidBucketNameException,
      InsufficientDataException, ErrorResponseException, InvalidFile {

    String fileName = UUID.randomUUID().toString();
    File file = File.createTempFile(fileName, ".png");

    fileName = client.uploadImage(file.getAbsolutePath(), ".png");
    client.getMedia(fileName);
  }

  @Test
  public void shouldReadAndWriteCorrectFileData()
      throws IOException, XmlPullParserException, NoSuchAlgorithmException, InvalidKeyException,
      InvalidArgumentException, InternalException, NoResponseException, InvalidBucketNameException,
      InsufficientDataException, ErrorResponseException, InvalidFile {

    // generate random data
    byte[] array = new byte[128];
    new Random().nextBytes(array);

    // random file with data
    String fileName = UUID.randomUUID().toString() + ".png";
    File file = File.createTempFile(fileName, ".png");

    BufferedWriter br = new BufferedWriter(new FileWriter(file));
    br.write(new String(array));
    br.flush();
    br.close();

    // write byte array
    fileName = client.uploadImage(file.getAbsolutePath(), ".png");
    System.out.println(fileName);

    // read file
    InputStream stream = client.getMedia(fileName);
    byte[] readFile = stream.readAllBytes();

    // assert array content is not empty
    byte[] emptyArray = new byte[128];
    assertFalse(Arrays.equals(readFile, emptyArray));

    // assert content is the same
    assertEquals(new String(array), new String(array));
  }
}
