package com.gitrekt.quora.exceptions;

import io.netty.handler.codec.http.HttpResponseStatus;

public class MethodNotAllowed extends ServerException {

  public MethodNotAllowed() {
    error = "Method not allowed";
  }

  @Override
  public String getMessage() {
    return error;
  }

  @Override
  public HttpResponseStatus getCode() {
    return HttpResponseStatus.METHOD_NOT_ALLOWED;
  }
}
