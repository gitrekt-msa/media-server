package com.gitrekt.quora.exceptions;

import io.netty.handler.codec.http.HttpResponseStatus;

public class InvalidFile extends ServerException {
    public InvalidFile() {
        error = "Invalid file uploaded.";
    }

    @Override
    public String getMessage() {
        return error;
    }

    @Override
    public HttpResponseStatus getCode() {
        return HttpResponseStatus.BAD_REQUEST;
    }
}
