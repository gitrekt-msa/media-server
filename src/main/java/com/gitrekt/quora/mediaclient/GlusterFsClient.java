package com.gitrekt.quora.mediaclient;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

public class GlusterFsClient {
  public String[] hosts;
  private final String volume;
  private int serverIdx;

  /** Singleton to work with GlusterFS. */
  public GlusterFsClient() {
    hosts = System.getenv("GLUSTER_HOST").split(":");
    volume = System.getenv("GLUSTER_VOL");
    serverIdx = 0;
  }

  /**
   * Create a new file.
   *
   * @param name name of the file to create
   * @throws IOException If file already exists.
   */
  public void createFile(String name) throws IOException {
    Set<PosixFilePermission> posixFilePermissions = PosixFilePermissions.fromString("rw-rw-rw-");
    FileAttribute<Set<PosixFilePermission>> attrs =
        PosixFilePermissions.asFileAttribute(posixFilePermissions);

    while (serverIdx < hosts.length) {
      try {
        Files.createFile(getFilePath(name), attrs);
        serverIdx = 0; // should do something smarter
        break;
      } catch (IllegalArgumentException exception) {
        serverIdx += 1;
      }
    }
  }

  /**
   * Create a new file with data.
   *
   * @param name name of the file.
   * @param data the data to write to the file.
   */
  public void writeFile(String name, byte[] data) throws IOException {
    while (serverIdx < hosts.length) {
      try {
        Files.write(getFilePath(name), data);
        serverIdx = 0; // should do something smarter
        break;
      } catch (IllegalArgumentException exception) {
        serverIdx += 1;
      }
    }
  }

  /**
   * Create a new file from file object.
   *
   * @param name file name to create.
   * @param file the file to get data from.
   */
  public void writeFile(String name, File file) throws IOException {
    while (serverIdx < hosts.length) {
      try {
        Files.write(getFilePath(name), Files.readAllBytes(Paths.get(file.getAbsolutePath())));
        serverIdx = 0; // should do something smarter
        return;
      } catch (IllegalArgumentException exception) {
        serverIdx += 1;
      }
    }
    serverIdx = 0;
  }

  /**
   * Read file.
   *
   * @param name name of the file
   * @return byte array of file contents.
   */
  public byte[] readFile(String name) throws IOException {
    while (serverIdx < hosts.length) {
      try {
        byte[] file = Files.readAllBytes(getFilePath(name));
        serverIdx = 0; // should do something smarter
        return file;
      } catch (IllegalArgumentException exception) {
        serverIdx += 1;
      }
    }

    serverIdx = 0;
    return null;
  }

  public boolean fileExists(String name) {
    boolean exists = false;
    while (serverIdx < hosts.length) {
      try {
        exists = Files.exists(getFilePath(name));
        serverIdx = 0; // should do something smarter
        return exists;
      } catch (IllegalArgumentException exception) {
        serverIdx += 1;
      }
    }

    serverIdx = 0;
    return exists;
  }
  /**
   * Get path object of the file.
   *
   * @param name path of the file.
   * @return
   */
  private Path getFilePath(String name) {
    String uri = String.format("mediaclient://%s:%s/%s", hosts[serverIdx], volume, name);
    return Paths.get(URI.create(uri));
  }


  private static class GlusterFsHelper {
    private static final GlusterFsClient INSTANCE = new GlusterFsClient();
  }

  public static GlusterFsClient getInstance() {
    return GlusterFsHelper.INSTANCE;
  }
}
