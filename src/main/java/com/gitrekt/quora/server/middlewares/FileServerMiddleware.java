package com.gitrekt.quora.server.middlewares;

import com.gitrekt.quora.exceptions.MethodNotAllowed;
import com.gitrekt.quora.exceptions.NotFoundException;
import com.gitrekt.quora.mediaclient.MinioMediaServer;
import com.gitrekt.quora.models.FileResponse;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.*;

import java.io.IOException;
import java.io.InputStream;

public class FileServerMiddleware extends SimpleChannelInboundHandler<HttpObject> {
//  GlusterFsClient glusterFs = GlusterFsClient.getInstance();
  MinioMediaServer minioMediaServer = MinioMediaServer.getInstance();

  @Override
  protected void channelRead0(ChannelHandlerContext ctx, HttpObject msg) throws Exception {

    if (msg instanceof HttpRequest) {
      HttpRequest httpRequest = (HttpRequest) msg;

      // get file only if get request
      if (!httpRequest.method().equals(HttpMethod.GET)) {
        throw new MethodNotAllowed();
      }

      // Decode Path
      QueryStringDecoder decoder = new QueryStringDecoder(httpRequest.uri());
      String[] path = decoder.path().substring(1).split("/");

      // Invalid Request
      if (path.length != 2 || !path[0].equals("media")) {
        throw new NotFoundException();
      }

      // read file
      InputStream file;
      try {
        file =  minioMediaServer.getMedia(path[1]);
      } catch (IOException exception) {
        throw new NotFoundException();
      }

      FileResponse response = new FileResponse();
      response.setFileName(path[1]);
      response.setData(file);
      response.setRequest(httpRequest);

      ctx.writeAndFlush(response).addListener(ChannelFutureListener.CLOSE);
    }
  }

  @Override
  public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    ctx.channel().writeAndFlush(cause).addListener(ChannelFutureListener.CLOSE);
  }
}
